<?php
/**
 *
 * SurveyMod
 *
 * @copyright (c) 2015 Wolfsblvt ( www.pinkes-forum.de )
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * @author Clemens Husung (Wolfsblvt)
 */

namespace bielu\surveymod\acp;

class surveymod_info
{
	function module()
	{
		return array(
			'filename'	=> '\bielu\surveymod\acp\surveymod_module',
			'title'		=> 'AP_TITLE_ACP',
			'modes'		=> array(
				'settings'	=> array('title' => 'AP_SETTINGS_ACP', 'auth' => 'ext_bielu/surveymod && acl_a_board', 'cat' => array('AP_TITLE_ACP')),
			),
		);
	}
}
